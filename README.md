This project was bootstrapped with [EXPO XDE]

It's a boilerplate for bootstrapping CRNA or EXPO Apps.

ATM it contains minimum screens and route configuration.

## Requirements
This project requires Expo. Whether you're running it on a real device or in a simulator, you'll need to download the Expo Client for the respective environment [android/ios].

You'll find the Expo guide & docs [here](https://docs.expo.io).

## Setup
* Clone the repository and navigate to it in your terminal
* Run `npm install` to install dependencies
* Run `expo start` to start the React Native packager - so that you can run/access the app
* Follow the instructions that show up in your terminal to run the app in any environment of your choice

## Note

This boilerplate is still a WIP,  PR's with Redux integration and basic forms are welcomed.

Cheers!