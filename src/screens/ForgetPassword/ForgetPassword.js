'use strict';

import React, {Component} from 'react';
import { View, StyleSheet } from 'react-native';
import {DisplayText} from '../../components';
import colors from '../../assets/colors';

export default class Register extends Component {
  constructor(props) {
    super(props);
   
    this.state = {

    };
   
  }

  componentDidMount() {
  }

  
  render () {
    return(
      <View style={styles.container}>  
  
          <DisplayText
            text={'welcome to Reset Password'}
            styles={StyleSheet.flatten(styles.text)}
            onPress = {()=>this.props.navigation.goBack()}
          />
  
      </View>
    )
   
  }
  
} 

const styles = StyleSheet.create({
  container: {
    flex:1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },

  text: {
    color: colors.gold,
    fontFamily: 'Montserrat-Bold',
    fontSize: 14,
    fontWeight: '200',
    marginLeft: 8,
  },
});
