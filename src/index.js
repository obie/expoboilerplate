'use strict';

import React, {Component} from 'react';
import { StyleSheet, View } from 'react-native';
import { Font } from 'expo';
import Navigator from './routes';

export default class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      fontsLoaded:false,
    };
  }

  componentDidMount() {

    (async() => {
      
      await Font.loadAsync({
      
        'Montserrat-Regular' : require('../src/assets/fonts/Montserrat-Regular.ttf'),
        'Montserrat-Bold' : require('../src/assets/fonts/Montserrat-Bold.ttf'),
      
      });
      
      this.setState({ fontsLoaded: true });
  
    })();
  }

  render() {
    const { fontsLoaded } = this.state

    return (
      <View style={styles.container}>
      {fontsLoaded ?
        <Navigator/>
        :
        null }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1, 
    justifyContent: 'center', 
    elevation: 4,
  },
});