import ForgetPassword from './ForgetPassword';
import Login from './Login';
import Register from './Register';
import Home from './Home';

export default {ForgetPassword, Login, Register, Home};