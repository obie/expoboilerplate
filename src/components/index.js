import { Alert , SingleButtonAlert} from './Alert';
import {SubmitButton} from './Buttons';
import {InputField}  from './TextInput';
import {Preloader}  from './Preloader';
import {DisplayText}  from './Text';
import  {BackIcon} from './Icons/BackIcon';



export {
  Alert , SingleButtonAlert, SubmitButton, InputField, Preloader, DisplayText, BackIcon
} 